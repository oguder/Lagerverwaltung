package oguderLagerverwaltung.Lagerverwaltung;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import commands.Initialisation;
import commands.TakeOut;

public class TakeOutTest {


	static TakeOut takeOut;
	static Warehouse warehouse = Initialisation.warehouse;
	static Article testArticle = new Article("Apfel", null, new ArticleNumber(11111) , null, null, 1.90);
	
	
	@Before
	public void create() {
		takeOut = new TakeOut();
		warehouse.getStorage()[1][0] = testArticle;
	}
	
	@Test
	public void takeOutArticle() {
		takeOut.execute(new ArticleNumber(11111));
		assertNull(warehouse.getStorage()[1][0]);
	}
	
	@Test
	public void takeOutArticleOnPostion() {
		takeOut.execute(1, 0);
		assertNull(warehouse.getStorage()[1][0]);
	}
}
