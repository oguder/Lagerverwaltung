package oguderLagerverwaltung.Lagerverwaltung;

import static org.junit.Assert.*;


import org.junit.BeforeClass;
import org.junit.Test;

import commands.*;

public class ConfigureTest {

	static Configure configure;
	static Warehouse warehouse = Initialisation.warehouse;
	
	@BeforeClass
	public static void create() {
		configure = new Configure();
	}
	
	@Test
	public void setSize() {
		assertEquals("2 2", warehouse.getSize());
		configure.setSize(2, 4);
		assertEquals("2 4", warehouse.getSize());
		assertEquals(2, warehouse.getStorage().length);
		assertEquals(4, warehouse.getStorage()[0].length);
		
	}
}
