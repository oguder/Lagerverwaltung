package oguderLagerverwaltung.Lagerverwaltung;

import commands.*;

import static org.junit.Assert.*;

import org.junit.*;

public class CommandTest{

	static CommandMenu menu;
	
	@BeforeClass
    public static void createMenu()
    {
		assertNull(menu);
		menu = new CommandMenu();
		menu.addComand(new Configure());
		menu.addComand(new Content());
		menu.addComand(new Help());
		menu.addComand(new Inventory());
		menu.addComand(new Location());
		menu.addComand(new StoreArticle());
		menu.addComand(new TakeOut());
		
       
       assertNotNull(menu);
    }

	
	@Test
	public void getCommandQuantity() {
		assertEquals(7, menu.getCommandQuantity());
	}
	
	@Test
	public void getCommands() {
		assertEquals(7, menu.getCommands().size());
	}
	
	
	
}
