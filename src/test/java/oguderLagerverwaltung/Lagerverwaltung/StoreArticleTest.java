package oguderLagerverwaltung.Lagerverwaltung;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import commands.Initialisation;
import commands.StoreArticle;

public class StoreArticleTest {

	static StoreArticle storeArticle;
	static Warehouse warehouse = Initialisation.warehouse;
	static Article testArticle = new Article("Apfel", null, new ArticleNumber(11111) , null, null, 1.90);
	
	
	@BeforeClass
	public static void create() {
		storeArticle = new StoreArticle();
		warehouse.getStorage()[0][0] = testArticle;
	}
	
	@Test
	public void storeArticleNextFree() {
		storeArticle.execute(testArticle);
		assertEquals(11111, warehouse.getStorage()[0][1].getArticleNumber());
	}
	
	@Test
	public void storeArticleDirect() {
		storeArticle.execute(testArticle, 1, 1);
		assertEquals(11111, warehouse.getStorage()[1][1].getArticleNumber());
		assertNull(warehouse.getStorage()[1][0]);
	}
}
