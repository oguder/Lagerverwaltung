package oguderLagerverwaltung.Lagerverwaltung;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;

import commands.Initialisation;
import commands.Location;

public class LocationTest {

	static Location location;
	static Warehouse warehouse = Initialisation.warehouse;
	static Article testArticle = new Article("Apfel", null, new ArticleNumber(11111) , null, null, 1.90);
	
	
	@BeforeClass
	public static void create() {
		location = new Location();
		warehouse.getStorage()[0][1] = testArticle;
	}
	
	@Test
	public void getLocationArticleNumber() {
		assertEquals(0, location.getPosition(testArticle.getArticleNumber()).getRow());
		assertEquals(1, location.getPosition(testArticle.getArticleNumber()).getColumn());
	}
	
	@Test
	public void getLocationArticle() {
		assertEquals(0, location.getPosition(testArticle).getRow());
		assertEquals(1, location.getPosition(testArticle).getColumn());
	}
	
}
