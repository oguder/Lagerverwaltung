package commands;

import oguderLagerverwaltung.Lagerverwaltung.Warehouse;


/**
 * Durch diese Klasse, kann die Größe des Lagers geändert werden.
 * @author guder
 *
 */
public class Configure implements Command{

	public void setSize(int row, int column) {
		Initialisation.warehouse.setSize(row, column);
		System.out.println(Initialisation.warehouse.getSize());

	}

	@Override
	public void execute() {
		System.out.println("Wieviele Reihen soll ihr neues Lager besitzen?");
		int row = Benutzereingabe.getUserInteger(1, Warehouse.MAX_SIZE);
		System.out.println("Wieviele Spalten soll ihr neues Lager besitzen?");
		int column = Benutzereingabe.getUserInteger(1, Warehouse.MAX_SIZE);
		
		setSize(row, column);
		
	}
	
	public String toString() {
		return "Configure";
	}
}
