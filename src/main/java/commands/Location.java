package commands;

import oguderLagerverwaltung.Lagerverwaltung.Article;
import oguderLagerverwaltung.Lagerverwaltung.ArticleNumber;
import oguderLagerverwaltung.Lagerverwaltung.Position;
import oguderLagerverwaltung.Lagerverwaltung.Warehouse;

/**
 * Diese Klasse liefert den Ort eines Artikels, sofern dieser im Lager vorhanden ist.
 * @author guder
 *
 */
public class Location implements Command{

	private Warehouse warehouse = Initialisation.warehouse;
	
	public Position getPosition(Article article) {
		Position position = new Position(-1, -1);
		for(int i = 0; i < warehouse.getStorage().length; i++) {
			for(int ii = 0; ii < warehouse.getStorage()[0].length; ii++) {
				if(warehouse.getArticle(i, ii) == article) {
					System.out.println("The article found in row " + i + " and column " + ii + ".");
					position = new Position(i, ii);
				}
			}
		}
		
		return position;
	}

	
	public Position getPosition(int articleNumber) {
		boolean found = false;
		Position position = new Position(-1, -1);
		for(int i = 0; i < warehouse.getStorage().length; i++) {
			for(int ii = 0; ii < warehouse.getStorage()[0].length; ii++) {
				if(warehouse.getStorage()[i][ii] != null) {
					if(warehouse.getArticle(i, ii).getArticleNumber() == articleNumber) {
						System.out.println("The article found in row " + i + " and column " + ii + ".");
						position = new Position(i, ii);
					}
				}
			}
		}
		
		if(!found) {
			System.out.println("Article not found.");
		}
		
		return position;
	}
	
	@Override
	public void execute() {
		System.out.println("Von welchem Artikel benötigen Sie den Platz? [Artikelnummer angeben:]");
		int articleNumber = Benutzereingabe.getUserInteger(0, ArticleNumber.MAX_NUMBER);
		
		System.out.println(getPosition(articleNumber));
		
	}
	
	@Override
	public String toString() {
		return "Location";
	}

}
