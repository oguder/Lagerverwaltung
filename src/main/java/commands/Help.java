package commands;

/**
 * Zeigt alle Befehle an, die zur Verfügung stehen.
 * @author guder
 *
 */
public class Help implements Command{

	public void execute() {
		Initialisation.menu.showCommands();
		
	}

	@Override
	public String toString() {
		return "Help";
	}
}
