package commands;

import oguderLagerverwaltung.Lagerverwaltung.Article;
import oguderLagerverwaltung.Lagerverwaltung.ArticleNumber;

public class TakeOut implements Command{

	public Article execute(int row, int column) {
		Article article;
		article = Initialisation.warehouse.getArticle(row, column);
		Initialisation.warehouse.getStorage()[row][column] = null;
		if(article == null) {
			throw new IllegalArgumentException();
		}
		return article;
	}
	
	public void execute(ArticleNumber articleNumber) {
		for(int i = 0; i < Initialisation.warehouse.getStorage().length; i++) {
			for(int ii = 0; ii < Initialisation.warehouse.getStorage()[0].length; ii++) {
				if(Initialisation.warehouse.getStorage()[i][ii] != null) {
					if(Initialisation.warehouse.getArticle(i, ii).getArticleNumber() == articleNumber.getValue()) {
						System.out.println("The article found in row " + i + " and column " + ii + ".");
						Initialisation.warehouse.clearPosition(i, ii);
					}
				}
			}
		}
	}

	@Override
	public void execute() {
		System.out.println("Welchen Artikel möchten Sie ausliefern?");
		ArticleNumber number = new ArticleNumber(Benutzereingabe.getUserInteger(0, ArticleNumber.MAX_NUMBER));
		execute(number);
		
	}
	
	@Override
	public String toString() {
		return "TakeOut";
	}
}
