package commands;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Durch die statischen Methoden der Klasse, erhällt man sichere Benutzereingaben
 * @author guder
 *
 */
public abstract class Benutzereingabe {

	private static Scanner scanner;
	
	/**
	 * 
	 * @param min Der Mindestwert, den der Benutzer eingeben kann (inklusiv)
	 * @param max Der Maximalwert, den der Benutzer eingeben kann (inklusiv)
	 * @return Die Benutzereingabe
	 */
	public static int getUserInteger(int min, int max) {
		if(min > max) {
			throw new IllegalArgumentException("Mindestwert: " + min + " | Maximalwert: " + max);
		}
		int value = 0;
		boolean gestartet = false;
		
		System.out.println("Geben Sie eine Zahl ein:\n");
		while(value < min || value > max || !gestartet) {
			if(gestartet) {
				System.out.println("Ungültige Eingabe");
			}
			gestartet = true;
			
			try {
				scanner = new Scanner(System.in);
				value = scanner.nextInt();
			}catch(InputMismatchException e) {
				System.out.println("Bitte eine Zahl eingeben!");
				value = min-1;
			}
		}
		return value;
	}
	
	
	
	public static double getUserDouble(int min, int max) {
		if(min > max) {
			throw new IllegalArgumentException("Mindestwert: " + min + " | Maximalwert: " + max);
		}
		double value = 0.00;
		boolean gestartet = false;
		
		System.out.println("Geben Sie eine Zahl ein:\n");
		while(value < min || value > max || !gestartet) {
			if(gestartet) {
				System.out.println("Ungültige Eingabe");
			}
			gestartet = true;
			
			try {
				scanner = new Scanner(System.in);
				value = scanner.nextDouble();
			}catch(InputMismatchException e) {
				System.out.println("Bitte eine Zahl eingeben!");
				value = min-1;
			}
		}
		return value;
	}
	
	public static boolean getUserBoolean() {
		String value = "";
		boolean gestartet = false;
		
		System.out.println("[y, yes, 1] oder [n, no, 0]\n");
		while(!value.equals("y") && !value.equals("yes") && !value.equals("1") &&
				!value.equals("n") && !value.equals("no") && !value.equals("0") || !gestartet) {
			value = "";
			if(gestartet) {
				System.out.println("Ungültige Eingabe\n");
			}
			gestartet = true;
			
			try {
				scanner = new Scanner(System.in);
				value = scanner.nextLine();
			}catch(InputMismatchException e) {
				System.out.println("Bitte einen gültigen Wert eingeben!");
			}
		}

		boolean bool = false;
		if(value.equals("y") || value.equals("yes") || value.equals("1")) {
			bool = true;
		}
		return bool;
	}
	
	/**
	 * Fordert vom Benutzer eine Eingabe.
	 * @param empty
	 * true - die Eingabe darf leer sein
	 * false - die Eingabe muss mindestens ein Zeichen enthalten
	 * @return Gibt Benutzereingabe als String zurueck
	 */
	public static String getUserString(boolean empty) {
		String value = "";
		boolean gestartet = false;
		
		System.out.println("[Benutzereingabe]\n");
		if(!empty) { //Eingabe darf nicht leer sein
			while(value.equals("") || !gestartet) {
				if(gestartet) {
					System.out.println("Ungültige Eingabe\n");
				}
				gestartet = true;
				
				try {
					scanner = new Scanner(System.in);
					value = scanner.nextLine();
				}catch(InputMismatchException e) {
					System.out.println("Bitte einen gültigen Wert eingeben!");
				}
			}
		}else { //Eingabe darf leer sein
			while(!gestartet) {
				if(gestartet) {
					System.out.println("Ungültige Eingabe\n");
				}
				gestartet = true;
				
				try {
					scanner = new Scanner(System.in);
					value = scanner.nextLine();
				}catch(InputMismatchException e) {
					System.out.println("Bitte einen gültigen Wert eingeben!");
				}
			}
		}
		return value;
	}
	
}
