package commands;

/**
 * Der Startpunkt der Konsole. Hier öffnet sich das Menü und von hier aus werden die Befehle gestartet.
 * @author guder
 *
 */
public class Console {

	public static void start() {
		
		int input = 1;
		while(input > 0) {
			Initialisation.menu.showCommands();
			
			System.out.println("Schließen = 0");
			input = Benutzereingabe.getUserInteger(0, Initialisation.menu.getCommandQuantity());
			if(input > 0) {
				Command command = Initialisation.menu.getCommands().get(input-1);
				command.execute();
			}
		}
		
	}
}
