package commands;

/**
 * Liefert den Inhalt des Lagers aus
 * @author guder
 *
 */
public class Inventory implements Command{
	
	public void execute() {
		System.out.println(Initialisation.warehouse.toString());
	}
	
	@Override
	public String toString() {
		return "Inventory";
	}
}
