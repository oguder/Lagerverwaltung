package commands;

import oguderLagerverwaltung.Lagerverwaltung.Article;
import oguderLagerverwaltung.Lagerverwaltung.Warehouse;

/**
 * Liefert den Artikel einer bestimmten Lagerposition.
 * Diese muss zuerst über Setter definiert werden.
 * @author guder
 *
 */
public class Content implements Command{

	public Article execute(int row, int column) {
		Article article = Initialisation.warehouse.getArticle(row, column);
		System.out.println(article);
		return article;
	}


	@Override
	public void execute() {
		System.out.println("Den Artikel den Sie möchten; in welcher Reihe liegt dieser?");
		int row = Benutzereingabe.getUserInteger(1, Warehouse.MAX_SIZE);
		System.out.println("Den Artikel den Sie möchten; in welcher Spalte liegt dieser?");
		int column = Benutzereingabe.getUserInteger(1, Warehouse.MAX_SIZE);
		
		execute(row, column);
	}
	
	@Override
	public String toString() {
		return "Content";
	}

}
