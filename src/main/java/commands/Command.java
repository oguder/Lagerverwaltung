package commands;

/**
 * Interface existiert, um Befehle zu einem Befehl zu machen und
 * diesen die Methode execute() zu übergeben.
 * @author guder
 *
 */
public interface Command {

	/**
	 * Über diese Methode können alle Befehle angesprochen werden
	 */
	void execute();

}
