package commands;

import oguderLagerverwaltung.Lagerverwaltung.Article;
import oguderLagerverwaltung.Lagerverwaltung.ArticleNumber;
import oguderLagerverwaltung.Lagerverwaltung.Description;
import oguderLagerverwaltung.Lagerverwaltung.Supplier;
import oguderLagerverwaltung.Lagerverwaltung.Warehouse;

public class StoreArticle implements Command{

	private Warehouse warehouse = Initialisation.warehouse;

	/**
	 * Lagert den Artikel am naechsten freien Platz ein.
	 * @param article - Den zu lagernden Artikel
	 */
	public void execute(Article article) {
		for(int i = 0; i < warehouse.getStorage().length; i++) {
			for(int ii = 0; ii < warehouse.getStorage()[0].length; ii++) {
				if(warehouse.getStorage()[i][ii] == null) {
					warehouse.getStorage()[i][ii] = article;
					return;
				}
			}
		}
		System.out.println("Lager ist voll!");
	}
	
	/**
	 * Lagert den Artikel an der angegebenen Stelle ein
	 * @param article - Den zu lagernden Artikel
	 * @param row - Die Reihe, in der der Artikel landen soll
	 * @param column - Die Spalte, in die der Artikel eingelagert wird
	 */
	public void execute(Article article, int row, int column) {
		if(warehouse.getStorage()[row][column] != null) {
			System.out.println("Lagerplatz ist besetzt!");
		}else {
			warehouse.getStorage()[row][column] = article;
		}
	}

	/**
	 * Laesst vom Benutzer ein Artikel erstellen und lagert dieses optional ein
	 */
	@Override
	public void execute() {
		System.out.println("Welchen Artikel möchten Sie einlagern? [Artikelnummer benötigt]");
		ArticleNumber articleNumber = new ArticleNumber(Benutzereingabe.getUserInteger(0, ArticleNumber.MAX_NUMBER));
		System.out.println("Wie lautet der Name Ihres Artikels " + articleNumber + "?");
		String name = Benutzereingabe.getUserString(false);
		System.out.println("Geben Sie die Beschreibung des Artikels " + name + " ein:");
		Description description = new Description(Benutzereingabe.getUserString(true));
		System.out.println("Über welchen Lieferanten erhalten Sie den Artikel?");
		Supplier supplier = new Supplier(Benutzereingabe.getUserString(false));
		System.out.println("Zu welcher Packeteinheit gehört das Paket?");
		String packageUnit = Benutzereingabe.getUserString(false);
		System.out.println("Wie hoch ist der Preis?");
		double price = Benutzereingabe.getUserDouble(0, 10000);
		
		Article article = new Article(name, description, articleNumber, supplier, packageUnit, price);
		
		System.out.println("Möchten Sie den Artikel " + articleNumber + " an einer bestimmten Stelle lagern?");
		if(Benutzereingabe.getUserBoolean()) {
			System.out.println("In welcher Reihe gehört der Artikel?");
			int row = Benutzereingabe.getUserInteger(0, warehouse.getRowNumber()-1);
			System.out.println("In welcher Spalte gehört der Artikel?");
			int column = Benutzereingabe.getUserInteger(0, warehouse.getColumnNumber()-1);
			execute(article, row, column);
			
		}else {
			execute(article);
		}
		
	}
	
	@Override
	public String toString() {
		return "StoreArticle";
	}
	
	
}
