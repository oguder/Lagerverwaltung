package commands;

import java.util.ArrayList;

/**
 * In dieser Klasse wird das Kommandomenü dargestellt
 * @author guder
 *
 */
public class CommandMenu {

	private ArrayList<Command> commands;

	
	public CommandMenu() {

		commands = new ArrayList<Command>();
		
	}
	
	/**
	 * Die Darstellung aller Befehle
	 */
	public void showCommands() {
		int counter = 1;
		System.out.println("------------------");
		System.out.println("Select a command:");
		
		for(Command command : commands) {
			System.out.println(counter++ + ") " + command.toString());
		}
		System.out.println("------------------");
	}
	
	/**
	 * Fuegt der Befehlsliste ein Element hinzu
	 * @param command - Der Befehl, der hinzugef�gt wird
	 */
	public void addComand(Command command) {
		commands.add(command);
	}
	
	/**
	 * Liefert die Anzahl an Befehlen
	 * @return Gibt ein int zurueck
	 */
	public int getCommandQuantity() {
		return commands.size();
	}

	public ArrayList<Command> getCommands() {
		return commands;
	}
	
	
	
	
	
	
}
