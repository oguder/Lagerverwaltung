package commands;

import oguderLagerverwaltung.Lagerverwaltung.Warehouse;

/**
 * Bringt das Programm in einen angemessenen Anfangszustand.
 * @author guder
 *
 */
public class Initialisation {

	public static Warehouse warehouse = new Warehouse(2, 2);
	public static CommandMenu menu = new CommandMenu();
	
	public static void initializeWarehouse() {
		
	}
	
	
	public static void initializeCommandMenue() {
		
		menu.addComand(new Configure());
		menu.addComand(new Content());
		menu.addComand(new Help());
		menu.addComand(new Inventory());
		menu.addComand(new Location());
		menu.addComand(new StoreArticle());
		menu.addComand(new TakeOut());
		
	}
}
