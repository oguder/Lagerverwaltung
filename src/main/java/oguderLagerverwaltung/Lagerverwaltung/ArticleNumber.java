package oguderLagerverwaltung.Lagerverwaltung;

public class ArticleNumber {

	public static final int MAX_NUMBER = 99999;
	
	private int value;
	
	public ArticleNumber(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	public String toString() {
		return "" + value;
	}
}
