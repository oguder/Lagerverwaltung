package oguderLagerverwaltung.Lagerverwaltung;

public class Warehouse {

	public static final int MAX_SIZE = 10000;
	
	private Article[][] storage;

	public Warehouse(int row, int column) {
		super();
		this.storage = new Article[row][column];
	}
	
	public Article[][] getStorage() {
		return storage;
	}

	public Article getArticle(int row, int column) {
		checkPosition(row, column);
		return storage[row][column];
	}
	
	public Article clearPosition(int row, int column) {
		checkPosition(row, column);
		Article article = storage[row][column];
		storage[row][column] = null;
		return article;
	}
	
	public void setArticle(int row, int column, Article article) {
		checkPosition(row, column);
		if(article == null) {
			throw new IllegalArgumentException("Article is null");
		}
		storage[row][column] = article;
	}
	
	public String getSize() {
		return storage.length + " " + storage[0].length;
	}
	
	public int getRowNumber() {
		return storage.length;
	}
	
	public int getColumnNumber() {
		return storage[0].length;
	}
	
	public void setSize(int row, int column) {
		checkPosition(row, column);
		Article[][] newStorage = new Article[row][column];
		for(int i = 0; i < storage.length && i < newStorage.length; i++) {
			for(int ii = 0; ii < storage[0].length && ii < newStorage[0].length; ii++) {
				newStorage[i][ii] = storage[i][ii];
			}
		}
		this.storage = newStorage;
	}
	
	public void checkPosition(int row, int column) throws IllegalArgumentException{
		if(row < 0 || row >= storage.length || column < 0 || column >= storage[0].length) {
			//throw new IllegalArgumentException("storage: " + getSize() + " | Parameter: " + row + " " + column);
		}
	}
	
	public String toString() {
		String output = "";
		
		for(int i = 0; i < storage.length; i++) {
			for(int ii = 0; ii < storage[0].length; ii++) {
				if(storage[i][ii] != null) {
					output += "---Row[" + i + "] | Column[" + ii + "]---\n " + storage[i][ii].toString() + "\n";
				}else {
					output += "---Row[" + i + "] | Column[" + ii + "]---\n X\n\n";
				}
			}
		}
		
		return output;
	}
}
