package oguderLagerverwaltung.Lagerverwaltung;

import commands.Console;
import commands.Initialisation;

public class App 
{
    public static void main( String[] args )
    {

    	Initialisation.initializeWarehouse();
    	Initialisation.initializeCommandMenue();
    	Console.start();
    }
}
