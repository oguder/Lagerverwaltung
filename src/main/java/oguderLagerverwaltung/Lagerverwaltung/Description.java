package oguderLagerverwaltung.Lagerverwaltung;

public class Description {

	private String text;

	public Description(String text) {
		super();
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public String toString() {
		return getText();
	}
}
