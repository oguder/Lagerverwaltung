package oguderLagerverwaltung.Lagerverwaltung;

public class Supplier {

	String name;

	public Supplier(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
	public String toString() {
		return getName();
	}
	
}
