package oguderLagerverwaltung.Lagerverwaltung;

public class Article {

	private String name;
	private Description description;
	private ArticleNumber number;
	private Supplier supplier;
	private String packageUnit;
	private Double price;
	
	
	public Article(String name, Description description, ArticleNumber number, Supplier supplier, String packageUnit, Double price) {
		super();
		this.name = name;
		this.description = description;
		this.number = number;
		this.supplier = supplier;
		this.packageUnit = packageUnit;
		this.price = price;
	}
	
	public int getArticleNumber() {
		return number.getValue();
	}
	
	public String getDescription() {
		return description.getText();
	}
	
	public String getSupplier() {
		return supplier.getName();
	}
	
	public String getPackageUnit() {
		return packageUnit;
	}
	
	public double getPrice() {
		return price;
	}
	
	public String toString(){
		return "Article: " + name + "\nDescription: " + description + "\nNumber: " + number + "\nSupplier: " + supplier + "\nPackageUnit: " + packageUnit + "\nPrice: " + price;
	}
	
}
